-- Your SQL goes here
CREATE FUNCTION insert_adresse(
arg_numero_voie integer,
arg_indice_repetition text,
arg_type_voie text,
arg_code_voie text,
arg_nom_voie text,
arg_code_postal integer,
arg_commune text,
arg_code_dept text,
arg_code_commune integer,
OUT adresse_id integer)
AS $$
DECLARE
BEGIN
INSERT INTO mowgli_adresse (numero_voie,
                         indice_repetition,
                         type_voie,
                         code_voie,
                         nom_voie,
                         code_postal,
                         commune,
                         code_dept,
                         code_commune)
       VALUES (arg_numero_voie,
               arg_indice_repetition,
               arg_type_voie,
               arg_code_voie,
               arg_nom_voie,
               arg_code_postal,
               arg_commune,
               arg_code_dept,
               arg_code_commune)
        ON CONFLICT ON CONSTRAINT adresse_identique
        DO UPDATE SET occurrences = mowgli_adresse.occurrences + 1
        RETURNING mowgli_adresse.id INTO adresse_id;
    RAISE NOTICE 'Inserted adresse : % ', adresse_id;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION insert_bien(
arg_adresse_id integer,
arg_prefixe_section text,
arg_id_section text,
arg_numero_plan integer,
OUT bien_id integer)
AS $$
DECLARE
BEGIN

INSERT INTO mowgli_bien (adresse_id,
                      prefixe_section,
                      id_section,
                      numero_plan)
       VALUES (arg_adresse_id,
               arg_prefixe_section,
               arg_id_section,
               arg_numero_plan)
        ON CONFLICT ON CONSTRAINT bien_identique
        DO UPDATE SET occurrences = mowgli_bien.occurrences + 1
        RETURNING mowgli_bien.id INTO bien_id;
RAISE NOTICE 'Inserted bien % ', bien_id;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION insert_subtrans(
       arg_adresse_id integer,
       arg_bien_id integer,
       arg_disposition_number integer,
       arg_mutation_date date,
       arg_mutation_kind text,
       arg_valeur_fonciere float,
       arg_numero_volume text,
       arg_lot_1 text,
       arg_surface_lot_1 float,
       arg_lot_2 text,
       arg_surface_lot_2 float,
       arg_lot_3 text,
       arg_surface_lot_3 float,
       arg_lot_4 text,
       arg_surface_lot_4 float,
       arg_lot_5 text,
       arg_surface_lot_5 float,
       arg_nombre_lots integer,
       arg_code_local integer,
       arg_type_local text,
       arg_surface_bati float,
       arg_nombre_pieces integer,
       arg_code_nature_culture text,
       arg_nature_culture_sp text,
       arg_surface_terrain float,
       OUT subtrans_id integer

) AS $$
DECLARE
BEGIN

INSERT INTO mowgli_subtransaction (adresse_id,
                                bien_id,
                                disposition_number,
                                mutation_date,
                                mutation_kind,
                                valeur_fonciere,
                                numero_volume,
                                lot_1,
                                surface_lot_1,
                                lot_2,
                                surface_lot_2,
                                lot_3,
                                surface_lot_3,
                                lot_4,
                                surface_lot_4,
                                lot_5,
                                surface_lot_5,
                                nombre_lots,
                                code_local,
                                type_local,
                                surface_bati,
                                nombre_pieces,
                                code_nature_culture,
                                nature_culture_sp,
                                surface_terrain)
        VALUES (
       arg_adresse_id,
       arg_bien_id,
       arg_disposition_number,
       arg_mutation_date,
       arg_mutation_kind,
       arg_valeur_fonciere,
       arg_numero_volume,
       arg_lot_1,
       arg_surface_lot_1,
       arg_lot_2,
       arg_surface_lot_2,
       arg_lot_3,
       arg_surface_lot_3,
       arg_lot_4,
       arg_surface_lot_4,
       arg_lot_5,
       arg_surface_lot_5,
       arg_nombre_lots,
       arg_code_local,
       arg_type_local,
       arg_surface_bati,
       arg_nombre_pieces,
       arg_code_nature_culture,
       arg_nature_culture_sp,
       arg_surface_terrain)
       RETURNING mowgli_subtransaction.id INTO subtrans_id;

RAISE NOTICE 'Inserted subtrans : % ', subtrans_id;

END;
$$ LANGUAGE plpgsql;



CREATE FUNCTION insert_line(
arg_numero_voie integer,
arg_indice_repetition text,
arg_type_voie text,
arg_code_voie text,
arg_nom_voie text,
arg_code_postal integer,
arg_commune text,
arg_code_dept text,
arg_code_commune integer,
arg_prefixe_section text,
arg_id_section text,
arg_numero_plan integer,
       arg_disposition_number integer,
       arg_mutation_date date,
       arg_mutation_kind text,
       arg_valeur_fonciere float,
       arg_numero_volume text,
       arg_lot_1 text,
       arg_surface_lot_1 float,
       arg_lot_2 text,
       arg_surface_lot_2 float,
       arg_lot_3 text,
       arg_surface_lot_3 float,
       arg_lot_4 text,
       arg_surface_lot_4 float,
       arg_lot_5 text,
       arg_surface_lot_5 float,
       arg_nombre_lots integer,
       arg_code_local integer,
       arg_type_local text,
       arg_surface_bati float,
       arg_nombre_pieces integer,
       arg_code_nature_culture text,
       arg_nature_culture_sp text,
       arg_surface_terrain float,
       OUT result_id integer

) AS $$
DECLARE
BEGIN

WITH
adresse_insert AS
     ( SELECT adresse_id FROM insert_adresse(arg_numero_voie,
                                    arg_indice_repetition,
                                    arg_type_voie,
                                    arg_code_voie,
                                    arg_nom_voie,
                                    arg_code_postal,
                                    arg_commune,
                                    arg_code_dept,
                                    arg_code_commune)),
bien_insert AS
     ( SELECT bien_id FROM insert_bien((SELECT adresse_id from adresse_insert),
                                 arg_prefixe_section,
                                 arg_id_section,
                                 arg_numero_plan))
SELECT subtrans_id FROM insert_subtrans((SELECT adresse_id from adresse_insert),
                              (SELECT bien_id from bien_insert),
                              arg_disposition_number,
                              arg_mutation_date,
                              arg_mutation_kind,
                              arg_valeur_fonciere,
                              arg_numero_volume,
                              arg_lot_1,
                              arg_surface_lot_1,
                              arg_lot_2,
                              arg_surface_lot_2,
                              arg_lot_3,
                              arg_surface_lot_3,
                              arg_lot_4,
                              arg_surface_lot_4,
                              arg_lot_5,
                              arg_surface_lot_5,
                              arg_nombre_lots,
                              arg_code_local,
                              arg_type_local,
                              arg_surface_bati,
                              arg_nombre_pieces,
                              arg_code_nature_culture,
                              arg_nature_culture_sp, arg_surface_terrain) INTO result_id;
END;
$$ LANGUAGE plpgsql;
