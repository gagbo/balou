use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fmt;
use std::fmt::Display;
use std::result::Result;

// TODO : implement Result type in GeocodingLayer as the return type of GeocodingLayer trait interface

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct Address {
    pub number: Option<u32>,
    pub repetition_index: Option<String>,
    pub street_name: Option<String>,
    pub post_code: Option<u32>,
    pub state: Option<String>,
    pub city_name: Option<String>,
    pub country: Option<String>,
    pub lat_lon: Option<(f32, f32)>,
}

impl Display for Address {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO : clean this up in a more functional way
        let mut repr = String::new();
        if let Some(number) = self.number {
            repr.push_str(number.to_string().as_str());
            repr.push(' ');
        }
        if let Some(repetition_index) = self.repetition_index.as_ref() {
            repr.push_str(repetition_index);
            repr.push(' ');
        }
        if let Some(street_name) = self.street_name.as_ref() {
            repr.push_str(street_name);
            repr.push(' ');
        }
        if let Some(post_code) = self.post_code {
            repr.push_str(post_code.to_string().as_str());
            repr.push(' ');
        }
        if let Some(city_name) = self.city_name.as_ref() {
            repr.push_str(city_name);
        }
        if let Some(state) = self.state.as_ref() {
            repr.push_str(", ");
            repr.push_str(state);
        }
        if let Some(country) = self.country.as_ref() {
            repr.push_str(", ");
            repr.push_str(country);
        }
        write!(f, "{}", repr)
    }
}

#[derive(Debug)]
pub enum GeocodingError {
    AddressNotFound(String),
    InternalError(String),
}

impl Display for GeocodingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            GeocodingError::AddressNotFound(_address) => write!(f, "Address not found"),
            GeocodingError::InternalError(_error) => write!(f, "Internal service error"),
        }
    }
}

impl Error for GeocodingError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
    fn description(&self) -> &str {
        match self {
            GeocodingError::AddressNotFound(_address) => "Address not found",
            GeocodingError::InternalError(_error) => "Internal service error",
        }
    }
}

pub trait GeocodingLayer {
    fn geocode(&self, hr_address: Address) -> Result<Address, GeocodingError>;
    fn batch_geocode(&self, hr_addresses: Vec<Address>) -> Result<Vec<Address>, GeocodingError>;
    fn is_healthy(&self) -> bool;
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        let test_addr = Address {
            number: Some(2),
            repetition_index: None,
            street_name: Some("résidence Géricault".to_owned()),
            post_code: Some(78150),
            state: Some("Ile De France".to_owned()),
            city_name: Some("Le Chesnay".to_owned()),
            country: Some("France".to_owned()),
            lat_lon: Some((48.829774, 2.131213)),
        };
        println!("Test Address is {}", test_addr);
        assert_eq!(2 + 2, 4);
    }
}
