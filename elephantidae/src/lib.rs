pub mod filters;
use anthophila::{Address, GeocodingLayer};
use chrono::NaiveDate;
pub use filters::{RangeFilter, TransactionQueryFilter};
use serde::{Deserialize, Serialize};
use uom::si::f64::Area;

mod errors;
pub use errors::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Transaction {
    pub surface: Area,
    pub price: i64,
    pub address: Address,
    pub building_type: BuildingType,
    pub date: NaiveDate,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum BuildingType {
    Unknown,
}

pub trait StorageLayer {
    fn get_transactions(filter: &TransactionQueryFilter) -> Result<Vec<Transaction>, Error>;
    fn geocode_addresses(&mut self, service: &impl GeocodingLayer) -> Result<u64, Error>;
    fn load_dvf_data(&mut self, input_stream: &impl std::io::BufRead) -> Result<u64, Error>;
    fn is_healthy(&self) -> bool;
}
