pub enum Error {
    InternalError(Box<dyn std::error::Error>),
    Unknown,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::InternalError(err) => write!(f, "internal error: {}", *err),
            _ => write!(f, "internal storage error"),
        }
    }
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::InternalError(err) => write!(f, "internal error: {:?}", *err),
            _ => write!(f, "internal storage error"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::InternalError(err) => Some(&**err),
            _ => None,
        }
    }
}
