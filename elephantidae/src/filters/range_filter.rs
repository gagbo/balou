// TODO: refactor to a builder pattern later

/// A Filter that allows checking if a value is within a range of values
#[derive(Clone, Debug, Copy)]
pub struct RangeFilter<T> {
    inclusive_min: bool,
    inclusive_max: bool,
    min_value: Option<T>,
    max_value: Option<T>,
}

impl<T> RangeFilter<T>
where
    T: PartialOrd + Clone,
{
    /// Return true if the value is inside the specified range
    pub fn has(&self, value: &T) -> bool {
        if let Some(max_value) = &self.max_value {
            if value > max_value || (value >= max_value && !self.inclusive_max) {
                return false;
            }
        }
        if let Some(min_value) = &self.min_value {
            if value < min_value || (value <= min_value && !self.inclusive_min) {
                return false;
            }
        }
        true
    }

    /// Add a constraint to a range for values that are <= max
    pub fn leq(mut old_range: Self, incl_max: &T) -> Self {
        old_range = RangeFilter {
            inclusive_max: true,
            max_value: Some(incl_max.clone()),
            ..old_range
        };
        old_range
    }

    /// Add a constraint to a range for values that are < max
    pub fn lt(mut old_range: Self, strict_max: &T) -> Self {
        old_range = RangeFilter {
            inclusive_max: false,
            max_value: Some(strict_max.clone()),
            ..old_range
        };
        old_range
    }

    /// Add a constraint to a range for values that are >= min
    pub fn geq(mut old_range: Self, incl_min: &T) -> Self {
        old_range = RangeFilter {
            inclusive_min: true,
            min_value: Some(incl_min.clone()),
            ..old_range
        };
        old_range
    }

    /// Add a constraint to a range for values that are > min
    pub fn gt(mut old_range: Self, strict_min: &T) -> Self {
        old_range = RangeFilter {
            inclusive_min: false,
            min_value: Some(strict_min.clone()),
            ..old_range
        };
        old_range
    }
}

impl<T> Default for RangeFilter<T>
where
    T: PartialOrd + Clone,
{
    /// Build a range that accepts any value.
    /// Use the default range as first argument to other RangeFilter builders
    fn default() -> Self {
        RangeFilter {
            inclusive_min: false,
            inclusive_max: false,
            min_value: None,
            max_value: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn leq() {
        let test_range: RangeFilter<i32> = RangeFilter::leq(RangeFilter::default(), &25);
        assert!(test_range.has(&12));
        assert!(test_range.has(&25));
        assert!(!test_range.has(&26));
    }
    #[test]
    fn lt() {
        let test_range: RangeFilter<f64> = RangeFilter::lt(RangeFilter::default(), &25.into());
        assert!(test_range.has(&12.into()));
        assert!(!test_range.has(&25.into()));
        assert!(!test_range.has(&26.into()));
    }
    #[test]
    fn geq() {
        let test_range: RangeFilter<u32> = RangeFilter::geq(RangeFilter::default(), &25);
        assert!(!test_range.has(&12));
        assert!(test_range.has(&25));
        assert!(test_range.has(&26));
    }
    #[test]
    fn gt() {
        let test_range: RangeFilter<i32> = RangeFilter::gt(RangeFilter::default(), &25);
        assert!(!test_range.has(&12));
        assert!(!test_range.has(&25));
        assert!(test_range.has(&26));
    }

    #[test]
    fn combine() {
        let incl_min_25: RangeFilter<u32> = RangeFilter::geq(RangeFilter::default(), &25);
        let test_range: RangeFilter<u32> = RangeFilter::lt(incl_min_25, &40);
        assert!(!test_range.has(&12));
        assert!(test_range.has(&25));
        assert!(test_range.has(&26));
        assert!(!test_range.has(&40));
        assert!(!test_range.has(&45));
    }
}
