pub mod range_filter;
pub use range_filter::RangeFilter;
use std::time::Duration;
use uom::si::f64::*;

#[derive(Debug, Clone, Copy)]
pub struct RectangleGeoFilter {
    lat_min: f32,
    lon_min: f32,
    lat_range: f32,
    lon_range: f32,
}

#[derive(Debug, Clone, Copy)]
pub struct CircleGeoFilter {
    lat_center: f32,
    lon_center: f32,
    radius: f32,
}

pub type Coordinates = (f32, f32);
#[derive(Debug, Clone)]
pub struct SegmentsGeoFilter {
    segments: Vec<Coordinates>,
}

#[derive(Debug, Clone)]
pub enum LatLonFilter {
    All,
    Rectangle(RectangleGeoFilter),
    Circle(CircleGeoFilter),
    Segments(SegmentsGeoFilter),
}

// Just a thing that excludes or includes elements in a list
#[derive(Debug, Clone)]
pub struct ListFilter<T: PartialEq> {
    data: std::marker::PhantomData<T>,
}

#[derive(Debug, Clone)]
pub struct TransactionQueryFilter {
    pub date_range: RangeFilter<Duration>,
    pub price_range: RangeFilter<i64>,
    pub surface_range: RangeFilter<Area>,
    pub type_filter: ListFilter<crate::BuildingType>,
    pub allowed_states: ListFilter<String>,
    pub allowed_towns: ListFilter<String>,
    pub geo_filter: LatLonFilter,
}
