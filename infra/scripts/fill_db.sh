#!/usr/bin/env bash

SCRIPTS_FOLDER=$(dirname "$0")

# Les arguments pour la base de données, l'hôte et le mot de passe sont
# définis par l'environnement mis en place dans le docker-compose.
# Le fichier de données a été téléchargé depuis cadastre.gouv.fr
"${SCRIPTS_FOLDER}"/../build/apps/db_filler localhost mowgli example immo ./data/sample.txt.gz
