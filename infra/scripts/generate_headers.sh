#!/usr/bin/env bash

SCRIPTS_FOLDER="$( cd "$(dirname "$0")" || exit ; pwd -P )"

"${SCRIPTS_FOLDER}"/./up.sh

pushd "${SCRIPTS_FOLDER}"/../db || exit
# The user (-u), password (-p) and database name (-d) are defined in
# docker-compose.yml with the environment variables of pg-mowgli builder
./ddl2cpp.py -u mowgli --host localhost -p example -d immo -s dvf -n mowglimmo -o ../include/mowglimmo/ddl2cpp

if command -v clang-format >/dev/null 2>&1; then
    clang-format -i ../include/mowglimmo/ddl2cpp/dvf/*
fi

popd || exit

"${SCRIPTS_FOLDER}"/./down.sh
