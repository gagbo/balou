#!/usr/bin/env bash

SCRIPTS_FOLDER="$( cd "$(dirname "$0")" || exit ; pwd -P )"

pushd "${SCRIPTS_FOLDER}"/../db || exit
docker-compose -p mowgli down --rmi local --remove-orphans
popd || exit
