use chrono;
use flate2::read::GzDecoder;
use std::iter::Iterator;
use std::{fs::File, io::Read, path::Path};

pub fn parse_file(path: &Path) -> std::io::Result<Vec<ParsedDvfLine>> {
    println!("Working on {}", path.to_str().unwrap_or("invalid path"));
    let mut text_gz = File::open(path)?;
    let mut text_lines = String::new();
    match path.extension().and_then(std::ffi::OsStr::to_str) {
        Some("gz") => {
            let mut text_file = GzDecoder::new(text_gz);
            text_file.read_to_string(&mut text_lines)?;
        }

        _ => {
            text_gz.read_to_string(&mut text_lines)?;
        }
    };
    Ok(text_lines
        .lines()
        .into_iter()
        .map(|dvf_line| {
            let dvf_line = dvf_line.replace(",", ".");
            parse_dvf_line(&dvf_line, &V1)
        })
        .collect())
}

#[derive(Debug)]
pub struct DvfFileFields<'a> {
    // _R suffix means "restricted" (should be empty)
    pub code_service_ch_r: usize,
    pub ref_document_r: usize,
    pub article_cgi_1_r: usize,
    pub article_cgi_2_r: usize,
    pub article_cgi_3_r: usize,
    pub article_cgi_4_r: usize,
    pub article_cgi_5_r: usize,
    pub no_disposition: usize,
    pub date_mutation: usize,
    pub nature_mutation: usize,
    pub valeur_fonciere: usize,
    pub no_voie: usize,
    pub indice_rep: usize,
    pub type_voie: usize,
    pub code_voie: usize,
    pub voie: usize,
    pub code_postal: usize,
    pub commune: usize,
    pub code_dept: usize,
    pub code_commune: usize,
    pub prefixe_section: usize,
    pub section: usize,
    pub no_plan: usize,
    pub no_volume: usize,
    pub lot_1: usize,
    pub surface_carrez_lot_1: usize,
    pub lot_2: usize,
    pub surface_carrez_lot_2: usize,
    pub lot_3: usize,
    pub surface_carrez_lot_3: usize,
    pub lot_4: usize,
    pub surface_carrez_lot_4: usize,
    pub lot_5: usize,
    pub surface_carrez_lot_5: usize,
    pub nombre_lots: usize,
    pub code_type_local: usize,
    pub type_local: usize,
    pub identifiant_local_r: usize,
    pub surface_reelle_bati: usize,
    pub nombre_pieces_principales: usize,
    pub code_nature_culture: usize,
    pub nature_culture_speciale: usize,
    pub surface_terrain: usize,
    pub number_of_fields: usize, // Used only for runtime checking the fields
    pub separator: &'a str,
}

pub const V1: DvfFileFields = DvfFileFields {
    // _R suffix means "restricted" (should be empty)
    code_service_ch_r: 0,
    ref_document_r: 1,
    article_cgi_1_r: 2,
    article_cgi_2_r: 3,
    article_cgi_3_r: 4,
    article_cgi_4_r: 5,
    article_cgi_5_r: 6,
    no_disposition: 7,
    date_mutation: 8,
    nature_mutation: 9,
    valeur_fonciere: 10,
    no_voie: 11,
    indice_rep: 12,
    type_voie: 13,
    code_voie: 14,
    voie: 15,
    code_postal: 16,
    commune: 17,
    code_dept: 18,
    code_commune: 19,
    prefixe_section: 20,
    section: 21,
    no_plan: 22,
    no_volume: 23,
    lot_1: 24,
    surface_carrez_lot_1: 25,
    lot_2: 26,
    surface_carrez_lot_2: 27,
    lot_3: 28,
    surface_carrez_lot_3: 29,
    lot_4: 30,
    surface_carrez_lot_4: 31,
    lot_5: 32,
    surface_carrez_lot_5: 33,
    nombre_lots: 34,
    code_type_local: 35,
    type_local: 36,
    identifiant_local_r: 37,
    surface_reelle_bati: 38,
    nombre_pieces_principales: 39,
    code_nature_culture: 40,
    nature_culture_speciale: 41,
    surface_terrain: 42,
    number_of_fields: 43, // Used only for runtime checking the fields
    separator: "|",
};

#[derive(Debug)]
pub struct ParsedDvfLine {
    pub disposition_number: Option<i32>,
    pub mutation_date: Option<chrono::NaiveDate>,
    pub mutation_kind: Option<String>,
    pub valeur_fonciere: Option<f64>,
    pub numero_voie: Option<i32>,
    pub indice_repetition: Option<String>,
    pub type_voie: Option<String>,
    pub code_voie: Option<String>,
    pub nom_voie: Option<String>,
    pub code_postal: Option<i32>,
    pub commune: Option<String>,
    pub code_dept: Option<String>,
    pub code_commune: Option<i32>,
    pub prefixe_section: Option<String>,
    pub id_section: Option<String>,
    pub numero_plan: Option<i32>,
    pub numero_volume: Option<String>,
    pub lot_1: Option<String>,
    pub surface_lot_1: Option<f64>,
    pub lot_2: Option<String>,
    pub surface_lot_2: Option<f64>,
    pub lot_3: Option<String>,
    pub surface_lot_3: Option<f64>,
    pub lot_4: Option<String>,
    pub surface_lot_4: Option<f64>,
    pub lot_5: Option<String>,
    pub surface_lot_5: Option<f64>,
    pub nombre_lots: Option<i32>,
    pub code_local: Option<i32>,
    pub type_local: Option<String>,
    pub surface_bati: Option<f64>,
    pub nombre_pieces: Option<i32>,
    pub code_nature_culture: Option<String>,
    pub nature_culture_sp: Option<String>,
    pub surface_terrain: Option<f64>,
}

fn str_to_option_str(input: &str) -> Option<&str> {
    match input {
        "" => None,
        _ => Some(input),
    }
}

fn str_to_option_string(input: &str) -> Option<String> {
    match input {
        "" => None,
        _ => Some(input.into()),
    }
}

impl ParsedDvfLine {
    pub fn from_vec_str<'b>(str_vector: Vec<&str>, dvf_version: &'b DvfFileFields) -> Self {
        ParsedDvfLine {
            disposition_number: str_vector[dvf_version.no_disposition].parse().ok(),
            mutation_date: chrono::NaiveDate::parse_from_str(
                str_vector[dvf_version.date_mutation],
                "%d/%m/%Y",
            )
            .ok(),
            mutation_kind: str_to_option_string(str_vector[dvf_version.nature_mutation]),
            valeur_fonciere: str_vector[dvf_version.valeur_fonciere].parse().ok(),
            numero_voie: str_vector[dvf_version.no_voie].parse().ok(),
            indice_repetition: str_to_option_string(str_vector[dvf_version.indice_rep]),
            type_voie: str_to_option_string(str_vector[dvf_version.type_voie]),
            code_voie: str_to_option_string(str_vector[dvf_version.code_voie]),
            nom_voie: str_to_option_string(str_vector[dvf_version.voie]),
            code_postal: str_vector[dvf_version.code_postal].parse().ok(),
            commune: str_to_option_string(str_vector[dvf_version.commune]),
            code_dept: str_to_option_string(str_vector[dvf_version.code_dept]),
            code_commune: str_vector[dvf_version.code_commune].parse().ok(),
            prefixe_section: str_to_option_string(str_vector[dvf_version.prefixe_section]),
            id_section: str_to_option_string(str_vector[dvf_version.section]),
            numero_plan: str_vector[dvf_version.no_plan].parse().ok(),
            numero_volume: str_to_option_string(str_vector[dvf_version.no_volume]),
            lot_1: str_to_option_string(str_vector[dvf_version.lot_1]),
            surface_lot_1: str_vector[dvf_version.surface_carrez_lot_1].parse().ok(),
            lot_2: str_to_option_string(str_vector[dvf_version.lot_2]),
            surface_lot_2: str_vector[dvf_version.surface_carrez_lot_2].parse().ok(),
            lot_3: str_to_option_string(str_vector[dvf_version.lot_3]),
            surface_lot_3: str_vector[dvf_version.surface_carrez_lot_3].parse().ok(),
            lot_4: str_to_option_string(str_vector[dvf_version.lot_4]),
            surface_lot_4: str_vector[dvf_version.surface_carrez_lot_4].parse().ok(),
            lot_5: str_to_option_string(str_vector[dvf_version.lot_5]),
            surface_lot_5: str_vector[dvf_version.surface_carrez_lot_5].parse().ok(),
            nombre_lots: str_vector[dvf_version.nombre_lots].parse().ok(),
            code_local: str_vector[dvf_version.code_type_local].parse().ok(),
            type_local: str_to_option_string(str_vector[dvf_version.type_local]),
            surface_bati: str_vector[dvf_version.surface_reelle_bati].parse().ok(),
            nombre_pieces: str_vector[dvf_version.nombre_pieces_principales]
                .parse()
                .ok(),
            code_nature_culture: str_to_option_string(str_vector[dvf_version.code_nature_culture]),
            nature_culture_sp: str_to_option_string(
                str_vector[dvf_version.nature_culture_speciale],
            ),
            surface_terrain: str_vector[dvf_version.surface_terrain].parse().ok(),
        }
    }

    pub fn get_sql_query_arg(&self, field_name: &str) -> String {
        match field_name {
            "disposition_number" => convert_options_to_sql_args(self.disposition_number),
            "mutation_date" => self.mutation_date.unwrap().format("'%Y-%m-%d'").to_string(),
            "mutation_kind" => convert_string_to_sql_args(self.mutation_kind.as_deref()),
            "valeur_fonciere" => convert_options_to_sql_args(self.valeur_fonciere),
            "numero_voie" => convert_options_to_sql_args(self.numero_voie),
            "indice_repetition" => convert_string_to_sql_args(self.indice_repetition.as_deref()),
            "type_voie" => convert_string_to_sql_args(self.type_voie.as_deref()),
            "code_voie" => convert_string_to_sql_args(self.code_voie.as_deref()),
            "nom_voie" => convert_string_to_sql_args(self.nom_voie.as_deref()),
            "code_postal" => convert_options_to_sql_args(self.code_postal),
            "commune" => convert_string_to_sql_args(self.commune.as_deref()),
            "code_dept" => convert_string_to_sql_args(self.code_dept.as_deref()),
            "code_commune" => convert_options_to_sql_args(self.code_commune),
            "prefixe_section" => convert_string_to_sql_args(self.prefixe_section.as_deref()),
            "id_section" => convert_string_to_sql_args(self.id_section.as_deref()),
            "numero_plan" => convert_options_to_sql_args(self.numero_plan),
            "numero_volume" => convert_string_to_sql_args(self.numero_volume.as_deref()),
            "lot_1" => convert_string_to_sql_args(self.lot_1.as_deref()),
            "surface_lot_1" => convert_options_to_sql_args(self.surface_lot_1),
            "lot_2" => convert_string_to_sql_args(self.lot_2.as_deref()),
            "surface_lot_2" => convert_options_to_sql_args(self.surface_lot_2),
            "lot_3" => convert_string_to_sql_args(self.lot_3.as_deref()),
            "surface_lot_3" => convert_options_to_sql_args(self.surface_lot_3),
            "lot_4" => convert_string_to_sql_args(self.lot_4.as_deref()),
            "surface_lot_4" => convert_options_to_sql_args(self.surface_lot_4),
            "lot_5" => convert_string_to_sql_args(self.lot_5.as_deref()),
            "surface_lot_5" => convert_options_to_sql_args(self.surface_lot_5),
            "nombre_lots" => convert_options_to_sql_args(self.nombre_lots),
            "code_local" => convert_options_to_sql_args(self.code_local),
            "type_local" => convert_string_to_sql_args(self.type_local.as_deref()),
            "surface_bati" => convert_options_to_sql_args(self.surface_bati),
            "nombre_pieces" => convert_options_to_sql_args(self.nombre_pieces),
            "code_nature_culture" => {
                convert_string_to_sql_args(self.code_nature_culture.as_deref())
            }
            "nature_culture_sp" => convert_string_to_sql_args(self.nature_culture_sp.as_deref()),
            "surface_terrain" => convert_options_to_sql_args(self.surface_terrain),
            _ => panic!("Unknown variable : {}", field_name),
        }
    }
}

fn split_dvf_line<'a, 'b>(line: &'a str, dvf_version: &'b DvfFileFields) -> Vec<&'a str> {
    let result: Vec<&str> = line.split(dvf_version.separator).collect();
    if result.len() != dvf_version.number_of_fields as usize {
        panic!(
            "Got the wrong number of fields : {} gave {} fields instead of {}",
            line,
            result.len(),
            dvf_version.number_of_fields
        );
    }
    result
}

pub fn parse_dvf_line<'b>(line: &str, dvf_version: &'b DvfFileFields) -> ParsedDvfLine {
    ParsedDvfLine::from_vec_str(split_dvf_line(line, dvf_version), dvf_version)
}

fn convert_options_to_sql_args<T: std::fmt::Display>(arg: Option<T>) -> String {
    match arg {
        Some(value) => format!("{}", value),
        None => "NULL".into(),
    }
}

fn convert_string_to_sql_args(arg: Option<&str>) -> String {
    match arg {
        Some(value) => format!("'{}'", value.replace("'", "''")),
        None => "NULL".into(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn line_split_with_last_field() {
        // Important note : the commas have been replaced with dots.
        let line = "|||||||000001|15/01/2018|Vente|160000.00|5053|||B041|CHARMOUX|1270|COLIGNY|01|108||AE|90||||||||||||0|1|Maison||159|5|S||500";
        let result = parse_dvf_line(line, &V1);
        assert!(result.disposition_number.is_some());
        assert_eq!(result.disposition_number.unwrap(), 1);
        assert!(result.mutation_date.is_some());
        assert_eq!(
            result.mutation_date.unwrap(),
            chrono::NaiveDate::from_ymd(2018, 1, 15)
        );
        assert!(result.mutation_kind.is_some());
        assert_eq!(result.mutation_kind.unwrap(), "Vente");
        assert!(result.valeur_fonciere.is_some());
        assert_eq!(result.valeur_fonciere.unwrap(), 160000.00);
        assert!(result.numero_voie.is_some());
        assert_eq!(result.numero_voie.unwrap(), 5053);
        assert!(result.code_voie.is_some());
        assert_eq!(result.code_voie.unwrap(), "B041");
        assert!(result.nom_voie.is_some());
        assert_eq!(result.nom_voie.unwrap(), "CHARMOUX");
        assert!(result.code_postal.is_some());
        assert_eq!(result.code_postal.unwrap(), 1270);
        assert!(result.commune.is_some());
        assert_eq!(result.commune.unwrap(), "COLIGNY");
        assert!(result.code_dept.is_some());
        assert_eq!(result.code_dept.unwrap(), "01");
        assert!(result.code_commune.is_some());
        assert_eq!(result.code_commune.unwrap(), 108);
        assert!(result.prefixe_section.is_none());
        assert!(result.id_section.is_some());
        assert_eq!(result.id_section.unwrap(), "AE");
        assert!(result.numero_plan.is_some());
        assert_eq!(result.numero_plan.unwrap(), 90);
        assert!(result.nombre_lots.is_some());
        assert_eq!(result.nombre_lots.unwrap(), 0);
        assert!(result.code_local.is_some());
        assert_eq!(result.code_local.unwrap(), 1);
        assert!(result.type_local.is_some());
        assert_eq!(result.type_local.unwrap(), "Maison");
        assert!(result.surface_bati.is_some());
        assert_eq!(result.surface_bati.unwrap(), 159.0);
        assert!(result.nombre_pieces.is_some());
        assert_eq!(result.nombre_pieces.unwrap(), 5);
        assert!(result.surface_terrain.is_some());
        assert_eq!(result.surface_terrain.unwrap(), 500.0);
    }

    #[test]
    fn line_split_no_last_field() {
        // Important note : the commas have been replaced with dots.
        let line = "|||||||000001|09/04/2018|Vente en l'état futur d'achèvement|1200000.00|||||||BOURG-EN-BRESSE|01|53||BR|398||1031||||||||||1||||||||";
        let result = parse_dvf_line(line, &V1);
        assert!(result.surface_terrain.is_none());
    }

    #[test]
    fn line_split_corse() {
        // Important note : the commas have been replaced with dots.
        let line = "|||||||000001|22/01/2018|Vente|2500.00||||B132|FORCALI|20250|CORTE|2B|96||G|78||||||||||||0||||||B|CHVER|884";
        let result = parse_dvf_line(line, &V1);
        assert_eq!(result.code_dept.unwrap(), "2B");
    }

    #[test]
    fn line_split_lots() {
        // Important note : the commas have been replaced with dots.
        let line = "|||||||000001|25/04/2018|Vente|857500.00|33||RUE|1614|CAULAINCOURT|75018|PARIS 18|75|118||AT|27||17|90.10|18||51||84||||4|2|Appartement||90|4|||";
        let result = parse_dvf_line(line, &V1);
        assert!(result.commune.is_some());
        assert_eq!(result.commune.unwrap(), "PARIS 18");
        assert!(result.lot_1.is_some());
        assert_eq!(result.lot_1.unwrap(), "17");
        assert!(result.surface_lot_1.is_some());
        assert_eq!(result.surface_lot_1.unwrap(), 90.10);
        assert!(result.lot_2.is_some());
        assert_eq!(result.lot_2.unwrap(), "18");
        assert!(result.surface_lot_2.is_none());
        assert!(result.lot_3.is_some());
        assert_eq!(result.lot_3.unwrap(), "51");
        assert!(result.surface_lot_3.is_none());
        assert!(result.lot_4.is_some());
        assert_eq!(result.lot_4.unwrap(), "84");
        assert!(result.surface_lot_4.is_none());
        assert!(result.nombre_lots.is_some());
        assert_eq!(result.nombre_lots.unwrap(), 4);
    }
}
