use oliphant::dvf_parsing::parse_file;
use std::{error::Error, path::PathBuf};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "parse_file", about = "A parser for dvf files.")]
struct Opt {
    /// Input file
    #[structopt(parse(from_os_str))]
    input: PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let opt = Opt::from_args();
    let test_result = parse_file(&opt.input)?;
    println!("{}", test_result.len());
    Ok(())
}
