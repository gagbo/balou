use crate::schema::public::oliphant_bien;
use serde::{Deserialize, Serialize};

#[derive(Insertable, Serialize)]
#[table_name = "oliphant_bien"]
pub struct NewBien<'a> {
    pub adresse_id: i32,
    pub prefixe_section: Option<&'a str>,
    pub id_section: Option<&'a str>,
    pub numero_plan: Option<i32>,
}

#[derive(Queryable, Identifiable, Deserialize, Serialize, Associations)]
#[table_name = "oliphant_bien"]
pub struct Bien {
    pub id: i32,
    pub adresse_id: i32,
    pub occurrences: Option<i32>,
    pub prefixe_section: Option<String>,
    pub id_section: Option<String>,
    pub numero_plan: Option<i32>,
}
