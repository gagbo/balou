use diesel::{r2d2::ConnectionManager, PgConnection};

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

mod adresse;
mod bien;
mod subtransaction;
pub use adresse::{Adresse, NewAdresse};
pub use bien::{Bien, NewBien};
pub use subtransaction::{NewSubtransaction, Subtransaction};
