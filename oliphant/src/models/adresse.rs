use crate::schema::public::oliphant_adresse;
use anthophila;
use serde::{Deserialize, Serialize};

#[derive(Insertable, Serialize)]
#[table_name = "oliphant_adresse"]
pub struct NewAdresse<'a> {
    pub numero_voie: Option<i32>,
    pub indice_repetition: Option<&'a str>,
    pub type_voie: Option<&'a str>,
    pub code_voie: Option<&'a str>,
    pub nom_voie: Option<&'a str>,
    pub code_postal: Option<i32>,
    pub commune: Option<&'a str>,
    pub code_dept: Option<&'a str>,
    pub code_commune: Option<i32>,
    pub latitude: Option<f32>,
    pub longitude: Option<f32>,
}

#[derive(Queryable, Identifiable, Deserialize, Serialize, Associations)]
#[table_name = "oliphant_adresse"]
pub struct Adresse {
    pub id: i32,
    pub occurrences: Option<i32>,
    pub numero_voie: Option<i32>,
    pub indice_repetition: Option<String>,
    pub type_voie: Option<String>,
    pub code_voie: Option<String>,
    pub nom_voie: Option<String>,
    pub code_postal: Option<i32>,
    pub commune: Option<String>,
    pub code_dept: Option<String>,
    pub code_commune: Option<i32>,
    pub latitude: Option<f32>,
    pub longitude: Option<f32>,
}

impl std::convert::TryInto<anthophila::Address> for Adresse {
    type Error = &'static str;

    fn try_into(self) -> Result<anthophila::Address, Self::Error> {
        Ok(anthophila::Address {
            number: self.numero_voie.map(|val| val as u32),
            repetition_index: self.indice_repetition,
            street_name: self.nom_voie,
            post_code: self.code_postal.map(|val| val as u32),
            state: self.code_dept,
            city_name: self.commune,
            country: Some("France".into()),
            lat_lon: Some((
                self.latitude.unwrap_or_default(),
                self.longitude.unwrap_or_default(),
            )),
        })
    }
}
