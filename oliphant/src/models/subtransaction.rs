use crate::schema::public::oliphant_subtransaction;
use chrono;
use serde::{Deserialize, Serialize};

#[derive(Insertable, Serialize)]
#[table_name = "oliphant_subtransaction"]
pub struct NewSubtransaction<'a> {
    pub adresse_id: i32,
    pub bien_id: i32,
    pub disposition_number: Option<i32>,
    pub mutation_date: Option<chrono::NaiveDate>,
    pub mutation_kind: Option<&'a str>,
    pub valeur_fonciere: Option<f64>,
    pub numero_volume: Option<&'a str>,
    pub lot_1: Option<&'a str>,
    pub surface_lot_1: Option<f64>,
    pub lot_2: Option<&'a str>,
    pub surface_lot_2: Option<f64>,
    pub lot_3: Option<&'a str>,
    pub surface_lot_3: Option<f64>,
    pub lot_4: Option<&'a str>,
    pub surface_lot_4: Option<f64>,
    pub lot_5: Option<&'a str>,
    pub surface_lot_5: Option<f64>,
    pub nombre_lots: Option<i32>,
    pub code_local: Option<i32>,
    pub type_local: Option<&'a str>,
    pub surface_bati: Option<f64>,
    pub nombre_pieces: Option<i32>,
    pub code_nature_culture: Option<&'a str>,
    pub nature_culture_sp: Option<&'a str>,
    pub surface_terrain: Option<f64>,
}

#[derive(Queryable, Identifiable, Deserialize, Debug, Serialize, Associations)]
#[belongs_to(super::Adresse)]
#[belongs_to(super::Bien)]
#[table_name = "oliphant_subtransaction"]
pub struct Subtransaction {
    pub id: i32,
    pub adresse_id: i32,
    pub bien_id: i32,
    pub disposition_number: Option<i32>,
    pub mutation_date: Option<chrono::NaiveDate>,
    pub mutation_kind: Option<String>,
    pub valeur_fonciere: Option<f64>,
    pub numero_volume: Option<String>,
    pub lot_1: Option<String>,
    pub surface_lot_1: Option<f64>,
    pub lot_2: Option<String>,
    pub surface_lot_2: Option<f64>,
    pub lot_3: Option<String>,
    pub surface_lot_3: Option<f64>,
    pub lot_4: Option<String>,
    pub surface_lot_4: Option<f64>,
    pub lot_5: Option<String>,
    pub surface_lot_5: Option<f64>,
    pub nombre_lots: Option<i32>,
    pub code_local: Option<i32>,
    pub type_local: Option<String>,
    pub surface_bati: Option<f64>,
    pub nombre_pieces: Option<i32>,
    pub code_nature_culture: Option<String>,
    pub nature_culture_sp: Option<String>,
    pub surface_terrain: Option<f64>,
}
