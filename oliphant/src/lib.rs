#![recursion_limit = "256"]
#[macro_use]
extern crate diesel;

pub mod dvf_parsing;
mod schema;

pub(crate) mod models;
