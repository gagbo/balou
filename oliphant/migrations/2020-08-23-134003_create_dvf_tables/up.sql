-- Creating the basics tables to read dvf files into
CREATE TABLE oliphant_adresse(
       -- Adresse
       id serial PRIMARY KEY,
       occurrences int DEFAULT(0),
       numero_voie int DEFAULT NULL,
       indice_repetition varchar(6) DEFAULT NULL,
       type_voie varchar(4) DEFAULT NULL,
       code_voie varchar(10) DEFAULT NULL,
       nom_voie varchar(40) DEFAULT NULL,
       code_postal int DEFAULT NULL,
       commune varchar(40) DEFAULT NULL,
       code_dept varchar(4) DEFAULT NULL,
       code_commune int DEFAULT NULL,
       latitude real DEFAULT NULL,
       longitude real DEFAULT NULL
);

CREATE TABLE oliphant_bien(
       -- Bien at a given address
       id serial PRIMARY KEY,
       adresse_id int NOT NULL REFERENCES oliphant_adresse(id),
       occurrences int DEFAULT(0),
       prefixe_section text DEFAULT NULL,
       id_section varchar(10) DEFAULT NULL,
       numero_plan int DEFAULT NULL
);

CREATE TABLE oliphant_subtransaction(
       -- Transaction from the dvf file
       -- Will list each subtransaction for a single building
       id serial PRIMARY KEY,
       adresse_id int NOT NULL REFERENCES oliphant_adresse(id),
       bien_id int NOT NULL REFERENCES oliphant_bien(id),
       disposition_number int DEFAULT NULL,
       mutation_date date,
       mutation_kind varchar(40) DEFAULT NULL,
       valeur_fonciere float DEFAULT NULL,
       numero_volume text DEFAULT NULL,
       lot_1 varchar(40) DEFAULT NULL,
       surface_lot_1 float DEFAULT NULL,
       lot_2 varchar(40) DEFAULT NULL,
       surface_lot_2 float DEFAULT NULL,
       lot_3 varchar(40) DEFAULT NULL,
       surface_lot_3 float DEFAULT NULL,
       lot_4 varchar(40) DEFAULT NULL,
       surface_lot_4 float DEFAULT NULL,
       lot_5 varchar(40) DEFAULT NULL,
       surface_lot_5 float DEFAULT NULL,
       nombre_lots int DEFAULT NULL,
       code_local int DEFAULT NULL,
       type_local varchar(40) DEFAULT NULL,
       surface_bati float DEFAULT NULL,
       nombre_pieces int DEFAULT NULL,
       code_nature_culture varchar(10) DEFAULT NULL,
       nature_culture_sp varchar(40) DEFAULT NULL,
       surface_terrain float DEFAULT NULL
);

CREATE INDEX adresse_dept ON oliphant_adresse USING BTREE (code_dept);
CREATE INDEX adresse_ville ON oliphant_adresse USING BTREE (code_postal, commune);

CREATE INDEX bien_adresse ON oliphant_bien USING BTREE (adresse_id);

CREATE INDEX subtransaction_adresse ON oliphant_subtransaction USING BTREE (adresse_id, mutation_date);
CREATE INDEX subtransaction_surface_bati ON oliphant_subtransaction USING BTREE (surface_bati);
CREATE INDEX subtransaction_surface_terrain ON oliphant_subtransaction USING BTREE (surface_terrain);
CREATE INDEX subtransaction_prix ON oliphant_subtransaction USING BTREE (valeur_fonciere);

ALTER TABLE oliphant_adresse
      ADD CONSTRAINT adresse_identique UNIQUE (code_postal,
                                                commune,
                                                code_dept,
                                                code_commune,
                                                nom_voie,
                                                code_voie,
                                                type_voie,
                                                indice_repetition,
                                                numero_voie);
ALTER TABLE oliphant_bien
      ADD CONSTRAINT bien_identique UNIQUE (adresse_id,
                                            prefixe_section,
                                            id_section,
                                            numero_plan);
