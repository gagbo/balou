use actix_web::{HttpResponse, http::StatusCode, Result};

/// Testing endpoint to see what requests look like
#[post("/test_dvf")]
pub fn load_dvf_file(req: String) -> Result<HttpResponse> {
    println!("Extracted : {:?}", req);
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../../static/welcome.html")))
}
