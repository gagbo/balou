use crate::models::{self, Pool};
use actix_session::Session;
use actix_web::{HttpRequest, HttpResponse, Result};
use diesel::{PgConnection, QueryDsl, RunQueryDsl};
//use crate::schema;
use actix_web::web;

// TODO : Filter the list of subtransactions
// TODO : Make the limit and argument of the endpoint
// TODO : Make the endpoint async to test tokio/futures
/// Query subtransactions from database
///
/// A "Subtransaction" is the smallest possible part of a transaction
/// This means 'one line in a government dvf file'. There may be multiple lines
/// per transaction in one dvf file, hence the "Subtransaction" name.
///
/// It is possible to have the same date, same amount, and even same adress in multiple lines with a
/// few lines describing one flat of the building, another for the commercial
/// surfaces, and another flat or two in the last few
///
/// # Example
/// GET /subtransaction
#[get("/subtransaction")]
pub fn subtrans(
    session: Session,
    req: HttpRequest,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, crate::errors::ServiceError> {
    use crate::schema::public::mowgli_subtransaction::dsl::*;
    println!("{:?}", req);
    let conn: &PgConnection = &pool.get().unwrap();
    let transactions = mowgli_subtransaction
        .limit(10)
        .load::<models::Subtransaction>(conn)?;

    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .header("X-Balou-Version", "1")
        .json(transactions))
}
