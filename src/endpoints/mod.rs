use actix_session::Session;
use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Result};

// Reminder to use the models and schema later
// use super::models;
// use super::schema;
pub mod dvf;
pub mod transaction;
pub use dvf::load_dvf_file;

/// Basic endpoint from actix tutorial
pub fn index() -> HttpResponse {
    HttpResponse::Ok().body("Hello Balou")
}

/// Basic endpoint from actix tutorial
pub fn index2() -> HttpResponse {
    HttpResponse::Ok().body("Hello Again")
}

/// Basic endpoint from actix tutorial
#[get("/welcome")]
pub fn welcome(session: Session, req: HttpRequest) -> Result<HttpResponse> {
    println!("{:?}", req);
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../../static/welcome.html")))
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{http, test};

    #[test]
    fn test_index_ok() {
        let resp = test::block_on(index()).unwrap();
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[test]
    fn test_index2_ok() {
        let resp = test::block_on(index2()).unwrap();
        assert_eq!(resp.status(), http::StatusCode::OK);
    }
}
