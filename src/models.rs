use super::schema::public::{mowgli_adresse, mowgli_bien, mowgli_subtransaction};
use chrono;
use diesel::{r2d2::ConnectionManager, PgConnection};
use serde::{Deserialize, Serialize};

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Insertable, Serialize)]
#[table_name = "mowgli_adresse"]
pub struct NewAdresse<'a> {
    pub numero_voie: Option<i32>,
    pub indice_repetition: Option<&'a str>,
    pub type_voie: Option<&'a str>,
    pub code_voie: Option<&'a str>,
    pub nom_voie: Option<&'a str>,
    pub code_postal: Option<i32>,
    pub commune: Option<&'a str>,
    pub code_dept: Option<&'a str>,
    pub code_commune: Option<i32>,
}

#[derive(Queryable, Identifiable, Deserialize, Serialize, Associations)]
#[table_name = "mowgli_adresse"]
pub struct Adresse {
    pub id: i32,
    pub occurrences: Option<i32>,
    pub numero_voie: Option<i32>,
    pub indice_repetition: Option<String>,
    pub type_voie: Option<String>,
    pub code_voie: Option<String>,
    pub nom_voie: Option<String>,
    pub code_postal: Option<i32>,
    pub commune: Option<String>,
    pub code_dept: Option<String>,
    pub code_commune: Option<i32>,
}

#[derive(Insertable, Serialize)]
#[table_name = "mowgli_bien"]
pub struct NewBien<'a> {
    pub adresse_id: i32,
    pub prefixe_section: Option<&'a str>,
    pub id_section: Option<&'a str>,
    pub numero_plan: Option<i32>,
}

#[derive(Queryable, Identifiable, Deserialize, Serialize, Associations)]
#[table_name = "mowgli_bien"]
pub struct Bien {
    pub id: i32,
    pub adresse_id: i32,
    pub occurrences: Option<i32>,
    pub prefixe_section: Option<String>,
    pub id_section: Option<String>,
    pub numero_plan: Option<i32>,
}

#[derive(Insertable, Serialize)]
#[table_name = "mowgli_subtransaction"]
pub struct NewSubtransaction<'a> {
    pub adresse_id: i32,
    pub bien_id: i32,
    pub disposition_number: Option<i32>,
    pub mutation_date: Option<chrono::NaiveDate>,
    pub mutation_kind: Option<&'a str>,
    pub valeur_fonciere: Option<f64>,
    pub numero_volume: Option<&'a str>,
    pub lot_1: Option<&'a str>,
    pub surface_lot_1: Option<f64>,
    pub lot_2: Option<&'a str>,
    pub surface_lot_2: Option<f64>,
    pub lot_3: Option<&'a str>,
    pub surface_lot_3: Option<f64>,
    pub lot_4: Option<&'a str>,
    pub surface_lot_4: Option<f64>,
    pub lot_5: Option<&'a str>,
    pub surface_lot_5: Option<f64>,
    pub nombre_lots: Option<i32>,
    pub code_local: Option<i32>,
    pub type_local: Option<&'a str>,
    pub surface_bati: Option<f64>,
    pub nombre_pieces: Option<i32>,
    pub code_nature_culture: Option<&'a str>,
    pub nature_culture_sp: Option<&'a str>,
    pub surface_terrain: Option<f64>,
}

#[derive(Queryable, Identifiable, Deserialize, Debug, Serialize, Associations)]
#[belongs_to(Adresse)]
#[belongs_to(Bien)]
#[table_name = "mowgli_subtransaction"]
pub struct Subtransaction {
    pub id: i32,
    pub adresse_id: i32,
    pub bien_id: i32,
    pub disposition_number: Option<i32>,
    pub mutation_date: Option<chrono::NaiveDate>,
    pub mutation_kind: Option<String>,
    pub valeur_fonciere: Option<f64>,
    pub numero_volume: Option<String>,
    pub lot_1: Option<String>,
    pub surface_lot_1: Option<f64>,
    pub lot_2: Option<String>,
    pub surface_lot_2: Option<f64>,
    pub lot_3: Option<String>,
    pub surface_lot_3: Option<f64>,
    pub lot_4: Option<String>,
    pub surface_lot_4: Option<f64>,
    pub lot_5: Option<String>,
    pub surface_lot_5: Option<f64>,
    pub nombre_lots: Option<i32>,
    pub code_local: Option<i32>,
    pub type_local: Option<String>,
    pub surface_bati: Option<f64>,
    pub nombre_pieces: Option<i32>,
    pub code_nature_culture: Option<String>,
    pub nature_culture_sp: Option<String>,
    pub surface_terrain: Option<f64>,
}
