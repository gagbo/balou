extern crate flate2;

use balou::dvf_parsing;
use diesel::{self, query_dsl::RunQueryDsl};
use flate2::read::GzDecoder;
use std::{
    env,
    fs::{self, File},
    io::Read,
    path::Path,
};

/// 'Bootstrap' the database with official government data
/// Path is hardcoded for now for testing purposes
fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let data_folder = Path::new(&args[1]);
    let pool = balou::configure_get_pool();


    for entry in fs::read_dir(data_folder)? {
        let path = entry?.path();
        // do NOT check the given folder recursively
        if path.is_dir() || !path.to_str().unwrap_or("invalid path").ends_with(".txt.gz") {
            continue;
        }
        let connection = pool.get().unwrap();
        println!("Working on {}", path.to_str().unwrap_or("invalid path"));
        let text_gz = File::open(path)?;
        let mut text_file = GzDecoder::new(text_gz);
        let mut text_lines = String::new();
        text_file.read_to_string(&mut text_lines)?;
        for (i, dvf_line) in text_lines.lines().enumerate() {
            // Official .txt.gz files have headers
            if i == 0 {
                continue;
            }
            let dvf_line = dvf_line.replace(",", ".");
            let parsed_line = dvf_parsing::parse_dvf_line(&dvf_line, &dvf_parsing::V1);
            add_line(&parsed_line, &connection).expect("Error inserting line");

            if i % 10000 == 0 {
                println!("Just did {} transactions", i);
            }
        }
    }

    Ok(())
}

/// Use the insert_line raw sql function to insert information from a new line in a gvt dvf file.
fn add_line(
    parsed_line: &dvf_parsing::ParsedDvfLine,
    connection: &diesel::PgConnection,
) -> Result<usize, diesel::result::Error> {
    let sql_function_call = format!(
        "
    SELECT * FROM insert_line(
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    {}, {});
",
        parsed_line.get_sql_query_arg("numero_voie"),
        parsed_line.get_sql_query_arg("indice_repetition"),
        parsed_line.get_sql_query_arg("type_voie"),
        parsed_line.get_sql_query_arg("code_voie"),
        parsed_line.get_sql_query_arg("nom_voie"),
        parsed_line.get_sql_query_arg("code_postal"),
        parsed_line.get_sql_query_arg("commune"),
        parsed_line.get_sql_query_arg("code_dept"),
        parsed_line.get_sql_query_arg("code_commune"),
        parsed_line.get_sql_query_arg("prefixe_section"),
        parsed_line.get_sql_query_arg("id_section"),
        parsed_line.get_sql_query_arg("numero_plan"),
        parsed_line.get_sql_query_arg("disposition_number"),
        parsed_line.get_sql_query_arg("mutation_date"),
        parsed_line.get_sql_query_arg("mutation_kind"),
        parsed_line.get_sql_query_arg("valeur_fonciere"),
        parsed_line.get_sql_query_arg("numero_volume"),
        parsed_line.get_sql_query_arg("lot_1"),
        parsed_line.get_sql_query_arg("surface_lot_1"),
        parsed_line.get_sql_query_arg("lot_2"),
        parsed_line.get_sql_query_arg("surface_lot_2"),
        parsed_line.get_sql_query_arg("lot_3"),
        parsed_line.get_sql_query_arg("surface_lot_3"),
        parsed_line.get_sql_query_arg("lot_4"),
        parsed_line.get_sql_query_arg("surface_lot_4"),
        parsed_line.get_sql_query_arg("lot_5"),
        parsed_line.get_sql_query_arg("surface_lot_5"),
        parsed_line.get_sql_query_arg("nombre_lots"),
        parsed_line.get_sql_query_arg("code_local"),
        parsed_line.get_sql_query_arg("type_local"),
        parsed_line.get_sql_query_arg("surface_bati"),
        parsed_line.get_sql_query_arg("nombre_pieces"),
        parsed_line.get_sql_query_arg("code_nature_culture"),
        parsed_line.get_sql_query_arg("nature_culture_sp"),
        parsed_line.get_sql_query_arg("surface_terrain"),
    );
    diesel::sql_query(sql_function_call).execute(connection)
}
