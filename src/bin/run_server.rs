use balou;

/// Run the http server
fn main() -> std::io::Result<()> {
    balou::run_http_server()
}
