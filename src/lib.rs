#![recursion_limit = "256"]
#[macro_use]
extern crate actix_web;
#[macro_use]
extern crate diesel;

// use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::{middleware, web, App, HttpServer};
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

pub mod dvf_parsing;
pub mod models;
pub mod schema;
pub mod errors;
mod endpoints;
use endpoints::{index, index2, welcome, load_dvf_file};

/// Get the configuration for the database and return a pool of connection to it
pub fn configure_get_pool() -> models::Pool {
    // Configuration
    dotenv::dotenv().ok();
    std::env::set_var("RUST_LOG", "actix_web=info,actix_server=info");
    env_logger::init();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    // let domain: String = std::env::var("DOMAIN").unwrap_or_else(|_| "localhost".to_string());

    // DB connection pool
    let manager = ConnectionManager::<PgConnection>::new(db_url);
    let pool: models::Pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create the database connection pool");
    pool
}

/// Run the http server to listen for requests
pub fn run_http_server() -> std::io::Result<()> {
    let pool = configure_get_pool();

    // Start HTTP Server
    HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .wrap(middleware::Logger::default())
            .route("/", web::get().to(index))
            .route("/again", web::get().to(index2))
            .service(welcome)
            .service(load_dvf_file)
            .service(endpoints::transaction::subtrans)
    }).bind("127.0.0.1:1990")?.run()

}
