use crate::models;
use anthophila::{Address, GeocodingError, GeocodingLayer};
use reqwest;

// TODO : better function for map_err

fn addr_to_query(address: &Address) -> String {
    format!("{}", address)
}

#[derive(Clone, Default)]
pub struct AddokService {
    url: String,
    client: reqwest::blocking::Client,
}

impl AddokService {
    pub fn new(url: String) -> Self {
        Self {
            url,
            client: reqwest::blocking::Client::new(),
        }
    }
}

impl GeocodingLayer for AddokService {
    fn geocode(&self, hr_address: Address) -> Result<Address, GeocodingError> {
        use reqwest::blocking::get;
        let response: models::FeatureCollection = get(&format!(
            "{}/search?q={}",
            self.url.as_str(),
            addr_to_query(&hr_address)
        ))
        .map_err(|_| GeocodingError::AddressNotFound(addr_to_query(&hr_address)))?
        .json()
        .map_err(|_| GeocodingError::InternalError("Payload is not a valid json".to_owned()))?;
        // TODO : put these answers in a logger
        // eprintln!("{:#?}", response);
        let mut answer = hr_address.clone();
        use std::str::FromStr;
        answer.lat_lon = match response.features[0].geometry.coordinates.as_slice() {
            [lon, lat] => Some((
                f32::from_str(lat.to_string().as_str()).expect("That f64 is not a valid f32"),
                f32::from_str(lon.to_string().as_str()).expect("That f64 is not a valid f32"),
            )),
            _ => None,
        };
        Ok(answer)
    }

    fn batch_geocode(&self, _hr_addresses: Vec<Address>) -> Result<Vec<Address>, GeocodingError> {
        todo!()
    }

    fn is_healthy(&self) -> bool {
        false
    }
}
