//! Models of the Addok response for easier communication.
//!
//! The spec lives [here](https://github.com/geocoders/geocodejson-spec/tree/master/draft)
//! Note that contrary to the spec, FeatureCollection metadata does not live in
//! its own namespace
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FeatureCollection {
    #[serde(rename = "type")]
    pub collection_type: String,
    pub version: String,
    pub features: Vec<Feature>,
    pub attribution: String,
    pub licence: String,
    pub query: String,
    pub limit: u64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Feature {
    #[serde(rename = "type")]
    pub feature_type: String,
    pub geometry: Geometry,
    pub properties: FeatureProperties,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Geometry {
    #[serde(rename = "type")]
    pub geo_type: String,
    pub coordinates: Vec<f64>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FeatureProperties {
    pub label: String,
    pub score: f64,
    pub housenumber: Option<String>,
    pub id: String,
    #[serde(rename = "type")]
    pub prop_type: String,
    pub x: f64,
    pub y: f64,
    pub importance: f64,
    pub name: String,
    pub postcode: String,
    pub citycode: String,
    pub oldcitycode: String,
    pub city: String,
    pub oldcity: String,
    pub context: String,
    pub street: String,
}

#[cfg(test)]
mod json_addok {
    use super::*;
    use serde_json;

    #[test]
    fn deserialize() {
        let input = r#"{
  "type": "FeatureCollection",
  "version": "draft",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          2.131213,
          48.829774
        ]
      },
      "properties": {
        "label": "2 Square Gericault 78150 Le Chesnay-Rocquencourt",
        "score": 0.479361988763586,
        "housenumber": "2",
        "id": "78158_0495_00002",
        "type": "housenumber",
        "x": 636222.31,
        "y": 6859208.27,
        "importance": 0.4917318763994462,
        "name": "2 Square Gericault",
        "postcode": "78150",
        "citycode": "78158",
        "oldcitycode": "78158",
        "city": "Le Chesnay-Rocquencourt",
        "oldcity": "Le Chesnay",
        "context": "78, Yvelines, Île-de-France",
        "street": "Square Gericault"
      }
    }
  ],
  "attribution": "BAN",
  "licence": "ETALAB-2.0",
  "query": "2 residence gericault 78158",
  "limit": 5
}"#;
        let _: FeatureCollection =
            serde_json::from_str(input).expect("Error during deserialization");
    }
}
