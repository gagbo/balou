use anthophila::{Address, GeocodingLayer};
use dotenv::dotenv;
use std::env;

mod addok_service;
mod models;

fn main() -> std::result::Result<(), std::boxed::Box<dyn std::error::Error>> {
    dotenv().ok();
    let addok_url = env::var("ADDOK_URL").expect("ADDOK_URL is unset!!");
    println!("Preparing to listen to {}", addok_url);
    let service = addok_service::AddokService::new(addok_url);
    let test_addr = Address {
        number: Some(2),
        repetition_index: None,
        street_name: Some("résidence Géricault".to_owned()),
        post_code: Some(78150),
        state: Some("Ile De France".to_owned()),
        city_name: Some("Le Chesnay".to_owned()),
        country: Some("France".to_owned()),
        lat_lon: None,
    };
    println!("{:?}", service.geocode(test_addr)?);
    Ok(())
}
